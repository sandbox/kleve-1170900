-- SUMMARY --

The Drupal hypercaster module enables the use of the HyperCaster player within
Drupal. The HyperCaster player is an interactive video player displaying
widgets on top of the videoscreen and the full project can be found at
http://sourceforge.net/projects/hypercaster/.

For a full description of the module, visit the project page:
  TODO

To submit bug reports and feature suggestions, or to track changes:
  TODO


-- REQUIREMENTS --

Drupal modules: CCK, Filefield

A copy of the HyperCaster player found at 
http://sourceforge.net/projects/hypercaster/

-- INSTALLATION --

Place the downloaded hypercaster folder within you hypercaster module folder
without renaming it.

After that, install the module as usual, see http://drupal.org/node/70151 for
further information.

-- CONFIGURATION --

You need to create 2 filefields and 1 textfield in the node type you want to
use the hypercaster in.

NOTE! In this version, the textfields machine-readable must have the following
name: field_hypercaster_xml

-- CUSTOMIZATION --

Go to admin/settings/hypercaster and choose your settings.

-- TROUBLESHOOTING --
TODO

-- FAQ --
TODO

-- CONTACT --

Current maintainers:
* Andreas Nilsson (Kleve) - http://drupal.org/user/786724
* Markus Broman (markusbroman) - http://drupal.org/user/675590
* Jonas Collins (HappyCloud) - http://drupal.org/user/614430

This project has been sponsored by:
* Flex @ Department of Computer and Systems Sciences, DSV (a part of Stockholm
  University).
  The flexible learning unit was started in 2010 as an indication for DSV:s
  commitment to new types of learning such as distance and blended learning.
  The unit is doing research in many different areas within the field such
  as: IT-pedagogy, learning platforms and LMS, simulation, and how video can be
  used in education. Besides research the unit is responsible for the delivery
  of all the distance courses at DSV and a number of curses in IT for learning
  including a master program in IT-didactics.
