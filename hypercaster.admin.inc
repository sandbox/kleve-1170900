<?php
/**
 * @file
 * Include file for HyperCaster administration page.
 *
 * Builds the administration form.
 *
 * Copyright (C) 2011 Andreas Nilsson, Markus Broman.
 *
 * Contact the developers:
 * Andreas Nilsson (Kleve) - http://drupal.org/user/786724
 * Markus Broman (markusbroman) - http://drupal.org/user/675590
 */


/**
 * HyperCaster administration page.
 * Builds the admin settings page.
 * @return
 *   system_settings_form($form)
 */
function hypercaster_admin(&$form_state) {

  $form['hypercaster'] = array(
    '#type' => 'fieldset',
    '#title' => t('Node and field settings'),
    '#description' => t("If you want to embed the player in another node than the one containing the video-files you need to enable Node Reference module and select a reference field pointing to that node."),
    '#collapsed' => FALSE,
  );

  //Get node types
  $node_types = array();
  foreach (node_get_types('types') as $key => $value) {
    $node_types[$key] = $key;
  }

  $form['hypercaster']['hypercaster_node_type'] = array(
    '#type' => 'select',
    '#title' => t('Node type to use for Hypercaster'),
    '#description' => t("Choose the node type you wish to embed Hypercaster player into."),
    '#options' => $node_types,
    '#default_value' => variable_get('hypercaster_node_type', $default),
  );

  $file_fields = hypercaster_get_fields_from_type('filefield');
  $reference_fields = hypercaster_get_fields_from_type('nodereference');
  $reference_fields['No reference'] = 'No reference';
  $text_fields = hypercaster_get_fields_from_type('text');

  $form['hypercaster']['hypercaster_reference_field'] = array(
    '#type' => 'select',
    '#title' => t('Reference field'),
    '#description' => t("If you use one node type for the player and one for the video files, choose the field that contains the reference to the node type with the file fields."),
    '#options' => $reference_fields,
    '#default_value' => variable_get('hypercaster_reference_field', $default),
  );

  $form['hypercaster']['hypercaster_default_playback_field'] = array(
    '#type' => 'select',
    '#title' => t('Default playback field'),
    '#description' => t("Choose the field containing the video."),
    '#options' => $file_fields,
    '#default_value' => variable_get('hypercaster_default_playback_field', $default),
  );

  $form['hypercaster']['hypercaster_thumbnail_field'] = array(
    '#type' => 'select',
    '#title' => t('Thumbnail field'),
    '#description' => t("Choose the field that contains video thumbnail."),
    '#options' => $file_fields,
    '#default_value' => variable_get('hypercaster_thumbnail_field', $default),
  );

  $form['hypercaster']['hypercaster_xml_field'] = array(
    '#type' => 'select',
    '#title' => t('XML field'),
    '#description' => t("Choose the field that contains the Hypercaster XML."),
    '#options' => $text_fields,
    '#default_value' => variable_get('hypercaster_xml_field', $default),
  );

  $form['hypercaster']['hypercaster_display_settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display settings'),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  for ($i=-50; $i < 51 ; $i++) {
    $weight[$i] = $i;
  }

  $form['hypercaster']['hypercaster_display_settings']['hypercaster_weight'] = array(
    '#type' => 'select',
    '#title' => t('Weight'),
    '#description' => t('Set the weight for the HyperCaster player and embed code on the node.'),
    '#options' => $weight,
    '#default_value' => variable_get('hypercaster_weight', $default),
  );

  return system_settings_form($form);
}

/**
 * A getter function used for retriveing fields.
 * @return
 *   system_settings_form($form)
 */
function hypercaster_get_fields_from_type($type) {

  $fields = content_fields();
  $fields_from_type = array();

  foreach ($fields as $field) {

    if ($field['type'] == $type) {
      $fields_from_type[$field['field_name']] = $field['widget']['label'];
    }
    elseif ($field['type'] == $type) {
      $fields_from_type[$field['field_name']] = $field['widget']['label'];
    }
    elseif ($field['type'] == $type) {
      $fields_from_type[$field['field_name']] = $field['widget']['label'];
    }
  }

  return $fields_from_type;
}